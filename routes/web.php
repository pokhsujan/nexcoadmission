<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('frontend/home/layout-home');
});
Auth::routes();

//Route::get('users', ['uses'=>'UsersController@index']);
//Route::get('users/create', ['uses'=>'UsersController@create']);
//Route::post('users/create', ['uses'=>'UsersController@store']);
//Route::get('users/edit/{id}', ['uses'=>'UsersController@edit']);
//Route::put('users/edit/{id}', ['uses'=>'UsersController@update']);
//Route::delete('users/delete/{id}', ['uses'=>'UsersController@destroy']);
//'middleware' => ['auth', 'verified']]


Route::group(['namespace' => 'Backend', 'prefix' => 'console', 'middleware' => 'auth'], function () {

    Route::group(['middleware' => ['role:admin']], function () {
        Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);

        Route::resource('users', 'UsersController');
    });


    Route::resource('pages', 'PagesController');


    //Route::get('dashboard', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);
    Route::get('my-profile', ['as' => 'users.profile', 'uses' => 'UsersController@myProfile']);
    Route::post('my-profile', ['as' => 'users.profile.update', 'uses' => 'UsersController@updateMyProfile']);
    Route::get('my-profile/logs', ['as' => 'users.mylogs', 'uses' => 'UsersController@myLogs']);
    Route::get('my-profile/change-password', ['as' => 'users.profile.change.password', 'uses' => 'UsersController@changePassword']);
    Route::post('my-profile/update-password', ['as' => 'users.profile.update.password', 'uses' => 'UsersController@updateMyPassword']);
});


Route::group(['namespace' => 'Frontend'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('pages/comment', ['uses' => 'PagesCotroller@comment', 'as' => 'pages.comment']);
    });

    Route::get('pages', ['uses' => 'PagesCotroller@index', 'as' => 'pages.index']);
    Route::get('register', ['uses' => 'UsersCotroller@create']);
    Route::get('login', ['uses' => 'UsersCotroller@login']);

});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
