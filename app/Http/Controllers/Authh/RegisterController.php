<?php

namespace App\Http\Controllers\Authh;

use Illuminate\Http\Request;
use App\Profile;
use App\User;

class RegisterController extends Controller
{
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
//        dd($data);
        return Validator::make($data, [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'full_name'=>'required|string|max:255',
//            'nickname'=>'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $guest = Role::whereName('user')->first();
        $user_data = [
            'username'=>$data['username'],
            'email'=>$data['email'],
            'password'=>bcrypt($data['password']),
            'status'=>1,
        ];
        $profile_data = [
            'full_name'=>$data['full_name'],
        ];
        $registered_user = User::create($user_data);
        $profile = new Profile($profile_data);
        $registered_user->profile()->save($profile);
        $registered_user->attachRole($guest);
        return $registered_user;
    }
}
