<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Page;
use App\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct(Page $page, User $user){
        $this->page=$page;
        $this->user=$user;
    }

    public function index(){
//        $pages= $this->page->published()->orderBy('view_count', 'DESC')->take(5)->get();
        $users = $this->user->latest('created_at')->take(5)->get();
        return view('backend.dashboard', compact( 'users'));
    }
}
