<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Profile;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{
    protected $user, $profile;

    public function __construct(User $user, Profile $profile)
    {
        $this->user = $user;
        $this->profile = $profile;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = $this->user->paginate(20);
//        $roles = $this->role->all();
        $users = $this->user->all();
        return view('backend.users.index', compact('users'));
    }

    public function create()
    {
        $users = User::all();
        return view('backend.users.create', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//    public function create()
//    {
//        return view('users.create');
//    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'full_name' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
//            'role' =>'required'
        ]);
        $user_data = [
            'name' => $request->full_name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'status' => 1
        ];
//        dd($user_data);
        $profile_data = $request->only('full_name', 'nickname');
        $this->user->create($user_data);
//        $profile = new $this->profile($profile_data);
//        $user->profile()->save($profile);
//        $user->roles()->sync('');
        return redirect()->route('users.index')->with('success', 'User Created Successfully.');

    }

    //delete function
    public function destroy($id)
    {
        $user = $this->user->findorFail($id);
        $user->delete();
        return redirect()->back();
    }

    //edit function to redirect to edit view
    public function edit($id)
    {
        $user = $this->user->findorFail($id);
//        $roles = $this->role->all();
        return view('backend.users.edit', compact('user'));
    }

    //update function
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'full_name' => 'required|string|max:255',
            'nickname' => 'required|string|max:255',
//            'role' =>'required'
        ]);
        $profile_data = [
            'full_name' => $request->input('full_name'),
            'nickname' => $request->input('nickname'),
            'description' => $request->input('description'),
            'avatar' => $request->input('avatar'),
            'display_name' => $request->input('display_name'),
            'website' => $request->input('website'),
        ];
        $user = $this->user->findorFail($id);
        $user->profile->update($profile_data);
        // $user->roles()->sync($request->input('role'));
        return redirect()->route('users.index')->with('success', 'User Updated Successfully.');;
    }

    //change Password view
    public function changePassword()
    {
        $user = $this->user->findorFail(auth()->user()->id);
        return view('backend.users.change-password', compact('user'));
    }

    //update password
    public function updatePassword(Request $request, $id)
    {
        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = $this->user->findorFail($id);
        $user->update(['password' => bcrypt($request->password)]);
        return redirect()->route('users.index');
    }

    public function myProfile()
    {
        $user = auth()->user();
        return view('backend.users.my-profile', compact('user'));
    }

    public function updateMyProfile(Request $request)
    {
        $this->validate($request, [
            'full_name' => 'required|string|max:255',
            'nickname' => 'required|string|max:255',
        ]);
        $profile_data = [
            'full_name' => $request->input('full_name'),
            'nickname' => $request->input('nickname'),
            'description' => $request->input('description'),
            'avatar' => $request->input('avatar'),
            'display_name' => $request->input('display_name'),
            'website' => $request->input('website'),
        ];
        $user = auth()->user();
        $user->profile->update($profile_data);
        return redirect()->route('users.profile')->with('success', 'Profile Successfully Updated!');

    }

    public function updateMyPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|string|min:6|confirmed',
        ]);
        $user = auth()->user();
        $user->update(['password' => bcrypt($request->password)]);
        return redirect()->route('users.profile')->with('success', 'Password Successfully Updated!');

    }

    public function activateUser($id)
    {
        $user = $this->user->findOrFail($id);
        $user->status = User::ACTIVE;
        $user->save();
        return redirect()->back()->with('success', 'User Successfully Banned!!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        $user = $this->modal->findOrFail($id);
////        return view('users.edit', compact($user));
//    }

}
