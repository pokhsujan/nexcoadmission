@extends('backend.layout')
@section('title'){{ __('Users') }} @endsection
@section('content')
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">User Nav</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">Users</li>
            </ol>
            <div class="card mb-4">
                <div class="card-body">

                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>username</th>
                                <th>Roles</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->username}}</td>
                                <td>{{$user->getRoleNames()}}</td>
                                {{-- <td>{{$user->hasRole('admin')}}</td>--}}
                                <td><a href="#" class="btn btn-info">Edit</a> <a href="#" class="btn btn-light">Delete</a></td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div style="height: 100vh;"></div>
        </div>
    </main>
@endsection
