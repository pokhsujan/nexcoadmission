{{ $user->username }}

{!! Form::open(array('route' => ['users.update',$user->id],  'method' => 'PUT', 'role' => 'form',  'required' => 'required')) !!}
<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-6">
                {!! Form::label('username', __('site.username')) !!}
                {!! Form::text('username',$user->username, array('class' => 'form-control', 'placeholder' => __('site.username'),'disabled' => 'disabled')) !!}
            </div>
            <div class="col-6">
                {!! Form::label('email', __('site.email')) !!}
                {!! Form::text('email',$user->email, array('class' => 'form-control', 'placeholder' => __('site.email'),'disabled' => 'disabled')) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('full_name', __('site.full_name')) !!}
            {!! Form::text('full_name',is_null($user->profile)?null:$user->profile->full_name, array('class' => 'form-control', 'placeholder' => __('site.full_name'))) !!}
        </div>
    </div>
{{--    <div class="col-md-5">--}}
{{--        <div class="form-group row">--}}
{{--            <div class="col-12">--}}
{{--                {!! Html::decode(Form::label('featured_image', __('site.avatar'), ['class' => 'control-label']))  !!}--}}
{{--                <div class="input-group">--}}
{{--                    {{ Form::text('avatar',(!is_null($user->profile) && !is_null($user->profile->avatar))?$user->profile->avatar:Gravatar::src($user->email),['class'=>'form-control', 'id'=>'thumbnail']) }}--}}
{{--                </div>--}}
{{--                <img id="holder" style="margin-top:15px;max-height:100px;" src="{{ (!is_null($user->profile) && !is_null($user->profile->avatar))?$user->profile->avatar:Gravatar::src($user->email) }}">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
</div>
<div class="row">
    <div class="col-md-7">
        <div class="form-group row">
            <div class="col-12">
                {!! Form::label('display_name', __('site.biographical_info')) !!}
                {{ Form::textarea('description', is_null($user->profile)?null:$user->profile->description, array('class' => 'form-control', 'placeholder' => __('site.user.few_details_about_yourself'))) }}
            </div>
        </div>
    </div>
    <div class="col-md-5">

        <div class="form-group">
            {!! Form::label('nickname', __('site.nickname')) !!}
            {!! Form::text('nickname',is_null($user->profile)?null:$user->profile->nickname, array('class' => 'form-control', 'placeholder' => __('site.nickname'))) !!}
        </div>
        <div class="form-group">
            {!! Form::label('display_name', __('site.display_name')) !!}
            {!! Form::text('display_name',is_null($user->profile)?null:$user->profile->display_name, array('class' => 'form-control', 'placeholder' => __('site.display_name'))) !!}
        </div>

        <div class="form-group">
            {!! Form::label('website',  __('site.user.website_url')) !!}
            {!! Form::text('website',is_null($user->profile)?null:$user->profile->website, array('class' => 'form-control', 'placeholder' => __('site.user.website_url'))) !!}
        </div>

{{--        <div class="form-group {{$errors->has('role') ? 'is-invalid':''}}">--}}
{{--            {!! Html::decode(Form::label('role', __('site.role.role').' <span class="required"> * </span>')) !!}--}}

{{--            @foreach($roles as $role)--}}
{{--                <div class="form-check">--}}
{{--                    <label class="custom-checkbox">--}}
{{--                        {{ Form::checkbox('role[]',$role->id,in_array($role->id, $user->roles()->pluck('id')->toArray()),['class'=>'custom-control-input']) }}--}}
{{--                        <span>{{ $role->display_name }}</span>--}}
{{--                    </label>--}}
{{--                </div>--}}
{{--            @endforeach--}}
{{--            @if ($errors->has('role'))--}}
{{--                <div class="invalid-feedback animated fadeInDown">--}}
{{--                    {{ $errors->first('role') }}--}}
{{--                </div>--}}
{{--            @endif--}}
        </div>
    </div>


</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success">
            <i class="fa fa-check"></i> {{ __('site.save') }}
        </button>
    </div>
</div>

{!! Form::close() !!}
