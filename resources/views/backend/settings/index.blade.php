@extends('backend.layout')
@section('title') {{__('site.setting.settings')}} @endsection
@push('styles')
@endpush
@section('content')
    <div class="page-subheader mb-0">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <div class="list">
                        <i class="fa fa-gear rounded-circle fs20 text-muted text-primary d-inline-block v-m"></i>
                        <span class="d-inline-block title-lg ml-3 v-m">{{__('site.setting.settings')}} - {{__('site.setting.all_settings')}}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-light-active">
        <div class="container-fluid">
            <div class="dad-tabs mt-5">
                <ul class="nav nav-pills" data-toggle="tabs" role="tablist" id="settingtabs">
                    @if(count(config('settings', [])) )
                        @php $k = 0; @endphp
                        @foreach(config('settings') as $section => $fields)
                            <li class="nav-item">
                                <a class="nav-link{{ $k==0?' active show':'' }}" href="#btabs-{{$section}}" aria-controls="btabs-{{$section}}"
                                   role="tab"
                                   data-toggle="tab" aria-selected="true">{{ $fields['title'] }}</a>
                            </li>
                            @php $k++; @endphp
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
    {!! Form::open(array('route' => ['settings.store'], 'method' => 'POST', 'role' => 'form','id' => 'settings-store', 'class'=>'mb-50')) !!}
    {!! Form::hidden('active_tab','btabs-general',['class'=>'hidden-tab-data']) !!}
    <div class="page-content d-flex flex">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="tab-content pt-3">
                        @if(count(config('settings', [])) )
                            @php $j = 0; @endphp
                            @foreach(config('settings') as $section => $fields)
                                <div class="tab-pane{{ $j==0?' active show':'' }}" id="btabs-{{ $section }}" role="tabpanel">
                                    <h4 class="font-w400"> {{ $fields['title'] }}</h4>
                                    <p class="text-muted">{{ $fields['description'] }}</p>
                                    <div class="form-body">
                                        @foreach($fields['elements'] as $field)
                                            @includeIf('backend.settings.fields._' . $field['type'] )
                                            @php $j++; @endphp
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">
                        <div class="block-options text-right">
                            <div class="block-options-item">
                                <button type="submit" class="btn btn-square btn-outline-success btn-sm"><i
                                        class="fa fa-check-circle"></i> {{ __('site.save') }} {{ __('site.setting.settings') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    @if ($errors->any())
                        <div class="alert alert-icon alert-danger mt-20" role="alert">
                            <i class="fa fa-info-circle"></i>
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <div class="future-ref">
{{--                        @if(setting('facebook_login') && !config('services.facebook.client_id') && !config('services.facebook.client_secret') && !config('services.facebook.redirect'))--}}
{{--                            <div class="alert alert-icon alert-danger mt-20" role="alert">--}}
{{--                                <i class="fa fa-info-circle"></i>--}}
{{--                                <strong>{{ __('site.setting.add_env') }}</strong>--}}
{{--                                <small class="form-text text-muted">FB_ID=Your Facebook API ID</small>--}}
{{--                                <small class="form-text text-muted">FB_SECRET=Your Facebook API SECRET</small>--}}
{{--                                <small class="form-text text-muted">FB_REDIRECT={{ url('social/handle/facebook') }} </small>--}}
{{--                                </br>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if(setting('twitter_login') && !config('services.twitter.client_id') && !config('services.twitter.client_secret') && !config('services.twitter.redirect'))--}}
{{--                            <div class="alert alert-icon alert-danger mt-20" role="alert">--}}
{{--                                <i class="fa fa-info-circle"></i>--}}
{{--                                <strong>{{ __('site.setting.add_env') }}</strong>--}}
{{--                                <small class="form-text text-muted">TW_ID=Your Twitter API ID</small>--}}
{{--                                <small class="form-text text-muted">TW_SECRET=Your Twitter API SECRET</small>--}}
{{--                                <small class="form-text text-muted">TW_REDIRECT={{ url('social/handle/twitter') }}</small>--}}
{{--                                </br>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if(setting('google_login') && !config('services.google.client_id') && !config('services.google.client_secret') && !config('services.google.redirect'))--}}
{{--                            <div class="alert alert-icon alert-danger mt-20" role="alert">--}}
{{--                                <i class="fa fa-info-circle"></i>--}}
{{--                                <strong>{{ __('site.setting.add_env') }}</strong>--}}
{{--                                <small class="form-text text-muted"> GOOGLE_ID=Your Google API ID</small>--}}
{{--                                <small class="form-text text-muted">GOOGLE_SECRET=Your Google API SECRET</small>--}}
{{--                                <small class="form-text text-muted">--}}
{{--                                    GOOGLE_REDIRECT={{ url('social/handle/google') }}</small>--}}
{{--                                </br>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if(!config('captcha.secret') && !config('captcha.sitekey'))--}}
{{--                            <div class="alert alert-icon alert-danger mt-20" role="alert">--}}
{{--                                <i class="fa fa-info-circle"></i>--}}
{{--                                <strong>{{ __('site.setting.add_env') }}</strong>--}}
{{--                                <small class="form-text text-muted"> CAPTCHA_SECRET=Your Recaptcha Secret</small>--}}
{{--                                <small class="form-text text-muted">CAPTCHA_SITEKEY=Your Recaptcha Site Key</small>--}}
{{--                                </br>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if(setting('comment_type') == 'disqus' && !config('disqus.enabled') && !config('disqus.username'))--}}
{{--                            <div class="alert alert-icon alert-danger mt-20" role="alert">--}}
{{--                                <i class="fa fa-info-circle"></i>--}}
{{--                                <strong>{{ __('site.setting.add_env') }}</strong>--}}
{{--                                <small class="form-text text-muted"> DISQUS_ENABLED=true</small>--}}
{{--                                <small class="form-text text-muted">DISQUS_USERNAME=Your Disqus Site Name</small>--}}
{{--                                </br>--}}
{{--                            </div>--}}
{{--                        @endif--}}
{{--                        @if(setting('mailchimp_subscription') && !config('newsletter.apiKey') && !config('newsletter.lists.subscribers.id'))--}}
{{--                            <div class="alert alert-icon alert-danger mt-20" role="alert">--}}
{{--                                <i class="fa fa-info-circle"></i>--}}
{{--                                <strong>{{ __('site.setting.add_env') }}</strong>--}}
{{--                                <small class="form-text text-muted">MAILCHIMP_APIKEY=your mailchimp api Key</small>--}}
{{--                                <small class="form-text text-muted">MAILCHIMP_LIST_ID=Your Mailchimp List ID</small>--}}
{{--                            </div>--}}
{{--                        @endif--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@endsection

{{--@push('modal')--}}
{{--    @include('backend.medias._modal-media-list')--}}
{{--@endpush--}}
@push('scripts')
    <script>
        $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
            var id = $(e.target).attr("href");
            localStorage.setItem('selectedTab', id)
        });
        var selectedTab = localStorage.getItem('selectedTab');
        if (selectedTab != null) {
            $('a[data-toggle="tab"][href="' + selectedTab + '"]').tab('show');
        }
    </script>
@endpush















