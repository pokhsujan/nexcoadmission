<div class="form-group {{ $errors->has($field['name']) ? ' is-invalid' : '' }}">
    <label for="{{ $field['name'] }}" class="control-label">{{ $field['label'] }}</label>
    {!! Form::text($field['name'],old($field['name'], \setting($field['name'])), ['class'=>'form-control','id'=>  $field['name'], 'placeholder'=> $field['label']]) !!}
    @if ($errors->has($field['name'])) <small class="invalid-feedback animated fadeInDown">{{ $errors->first($field['name']) }}</small> @endif
</div>
