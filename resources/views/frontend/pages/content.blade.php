@extends('frontend/pages/layout-page')
@section('title'){{ __('Nexcoo') }} @endsection
@section('content')
    <!-- Image Showcases -->
    <section class="showcase">
        <div class="container-fluid p-0">

            <div class="row no-gutters">
                <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/WSU.jpg');"></div>
                <div class="col-lg-6 my-auto showcase-text">
                    <a href="https://www.westernsydney.edu.au"><h2>Western Sydney University</h2></a>
                    <p class="lead mb-0">Western Sydney University is a world-class university, globally recognised for its research strengths and innovations in teaching. Here's why international students from over 70 countries choose to study at Western Sydney University every year.</p>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/victoria.jpg');"></div>
                <div class="col-lg-6 order-lg-1 my-auto showcase-text">
                    <a href="https://www.vu.edu.au/">  <h2>Victoria University</h2></a>
                    <p class="lead mb-0">At Victoria University there's more than one way to succeed at learning. We provide multiple unique ways to achieve a world-class education and an exciting career.

                        Different ways of learning include:

                        work placements with our industry partners
                        the VU Block Model – with smaller classes, focused learning and better results
                        employability programs that prepare you for work after graduation.</p>
                </div>
            </div>
        </div>
    </section>
@endsection
