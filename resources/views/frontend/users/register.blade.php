@extends('frontend/pages/layout-page')
@section('title'){{ __('Register User') }} @endsection
@section('content')

    <section class="showcase">
    {!! Form::open(array('route' => 'users.store',  'method' => 'POST', 'role' => 'form',  'required' => 'required')) !!}
    <div class="form-group {{$errors->has('username') ? 'is-invalid':''}}">
        {!! Html::decode(Form::label('username', 'User Name'.' <span class="required"> * </span>')) !!}
        {!! Form::text('username',old('username'), array('class' => 'form-control', 'placeholder' => __('site.username'))) !!}
        @if ($errors->has('username'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('username') }}
            </div>
        @endif
    </div>

    <div class="form-group {{$errors->has('email') ? 'is-invalid':''}}">
        {!! Html::decode(Form::label('email', __('site.email').' <span class="required"> * </span>')) !!}
        {!! Form::text('email',old('email'), array('class' => 'form-control', 'placeholder' => __('site.email'))) !!}
        @if ($errors->has('email'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('email') }}
            </div>
        @endif
    </div>

    <div class="form-group {{$errors->has('full_name') ? 'is-invalid':''}}">
        {!! Html::decode(Form::label('full_name', __('site.full_name').' <span class="required"> * </span>')) !!}
        {!! Form::text('full_name',old('username'), array('class' => 'form-control', 'placeholder' => __('site.full_name'))) !!}
        @if ($errors->has('full_name'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('full_name') }}
            </div>
        @endif
    </div>

{{--    <div class="form-group {{$errors->has('nickname') ? 'is-invalid':''}}">--}}
{{--        {!! Html::decode(Form::label('nickname', __('site.nickname').' <span class="required"> * </span>')) !!}--}}
{{--        {!! Form::text('nickname',old('nickname'), array('class' => 'form-control', 'placeholder' => __('site.nickname'))) !!}--}}
{{--        @if ($errors->has('nickname'))--}}
{{--            <div class="invalid-feedback animated fadeInDown">--}}
{{--                {{ $errors->first('nickname') }}--}}
{{--            </div>--}}
{{--        @endif--}}
{{--    </div>--}}

    <div class="form-group {{$errors->has('password') ? 'is-invalid':''}}">
        {!! Html::decode(Form::label('password', __('site.password').' <span class="required"> * </span>')) !!}
        {!! Form::password('password',array('class' => 'form-control', 'placeholder' => __('site.password'))) !!}
        @if ($errors->has('password'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('password') }}
            </div>
        @endif
    </div>

    <div class="form-group {{$errors->has('password_confirmation') ? 'is-invalid':''}}">
        {!! Form::label('password_confirmation', __('site.confirm_password')) !!}
        {!! Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' =>  __('site.confirm_password'))) !!}
        @if ($errors->has('password_confirmation'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('password_confirmation') }}
            </div>
        @endif
    </div>

    <div class="form-group {{$errors->has('role') ? 'is-invalid':''}}">
        {!! Html::decode(Form::label('role', __('site.role.role').' <span class="required"> * </span>')) !!}

        @foreach($roles as $role)
            <div class="form-check">
                <label class="custom-checkbox">
                    {{ Form::checkbox('role[]',$role->id,'',['class'=>'custom-control-input']) }}
                    <span>{{ $role->display_name }}</span>
                </label>
                <br>
            </div>
        @endforeach
        @if ($errors->has('role'))
            <div class="invalid-feedback animated fadeInDown">
                {{ $errors->first('role') }}
            </div>
        @endif
    </div>


    <div class="form-group">
        {!! Form::button(__('site.save'), array('class' => 'btn btn-outline-success btn-block','type' => 'submit')) !!}
    </div>
    {!! Form::close() !!}
    </section>

@endsection
