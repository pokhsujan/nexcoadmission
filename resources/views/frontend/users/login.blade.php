@extends('frontend/pages/layout-page')
@section('title'){{ __('Login User') }} @endsection
@section('content')

    <section class="showcase">
        <form method="POST" action="{{ route('login') }}" aria-label="Sign In">
            @csrf
            <div class="form-group">
                <input id="email" type="text"
                       class="form-control form-control-lg{{ $errors->has('email') ? ' is-invalid' : '' }}"
                       name="email" placeholder="Username/Email" value="{{ old('email') }}"
                       required autofocus>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errorsw->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <input id="password" type="password"
                       class="form-control form-control-lg{{ $errors->has('password') ? ' is-invalid' : '' }}"
                       name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <label for="check-remember" class="custom-checkbox mb-3 checkbox-dark d-block">
                <input id="check-remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                <span class="label-helper"> Remember Me</span>
            </label>
            <button type="submit" class="btn btn-primary btn-block btn-lg btn-icon">Log In</button>
        </form>
        <div class="text-center mt-10">
            Forget your Password?
            <a href="{{ route('password.request') }}" class="text-primary ml-2 b-b d-inline-block">Reset Password</a>
        </div>
    </section>
@endsection
